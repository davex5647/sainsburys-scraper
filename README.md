# Sainsburys Product Scraper

This application scrapes the Ripe Fruits page of the sainsburys website and returns the products as JSON.

## Requirements

Please download and extract PhantomJS: http://phantomjs.org/download.html

Set the path to the PhantomJS executable in the application properties file _product-scraper.properties_

## Running the Application

This is a command line application. The main method is in Application.java.
The application uses Maven and can be run with the command:

> mvn clean compile exec:java

## Notes on performance

I'm afraid it takes a long time to run - the initial load of the products page is fairly quick, but then visiting each individual product page takes a while.

Given more time I'd use concurrency to improve performance and possibly look at other libraries.

The text output it also quite verbose from phantomJS - the final JSON is at the end of the output.


