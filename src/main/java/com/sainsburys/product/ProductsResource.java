package com.sainsburys.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class ProductsResource {

    @JsonProperty("results")
    private List<ProductResource> products;

    public ProductsResource(List<ProductResource> products) {
        this.products = products;
    }

    public List<ProductResource> getProducts() {
        return products;
    }

    @JsonProperty("total")
    public double getTotal() {
        return products.stream().mapToDouble(i -> i.getUnitPrice()).sum();
    }

    public static String getJson(ProductsResource products) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writeValueAsString(products);
        } catch (JsonProcessingException e) {
            return "";
        }
    }

}
