package com.sainsburys.product;

import com.sainsburys.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;


public class ProductReader {

    /**
     * Scrapes a page of products, following each product page link to get its description
     * @return ProductsResource
     */
    public ProductsResource getProductsFromPage(String url) {
        WebDriver driver = WebDriverHelper.getWebDriver();
        try {
            driver.get(url);
            return scrapeProductPage(driver);
        } finally {
            driver.quit();
        }
    }

    private ProductsResource scrapeProductPage(WebDriver driver) {
        List<WebElement> products = driver.findElements(By.className("product"));
        List<ProductResource> productResources = new ArrayList<>();

        for (WebElement product : products) {
            ProductResource productResource = createProductResource(product);
            productResources.add(productResource);
        }

        return new ProductsResource(productResources);
    }

    private ProductResource createProductResource(WebElement product) {
        WebElement productInfo = product.findElement(By.className("productInfo"));
        String title = productInfo.getText();

        // get the link to the product info page
        WebElement h3 = product.findElement(By.tagName("h3"));
        WebElement a = h3.findElement(By.tagName("a"));
        String productHref = a.getAttribute("href");

        WebElement pricingInfo = product.findElement(By.className("pricePerUnit"));
        String pricePerUnit = pricingInfo.getText().replaceAll("/unit","");

        ProductResource productResource = new ProductResource(title, pricePerUnit);
        setProductDescriptionAndSize(productResource, productHref);

        return productResource;
    }

    private void setProductDescriptionAndSize(ProductResource product, String productHref) {
        WebDriver driver = WebDriverHelper.getWebDriver();
        try {
            driver.get(productHref);
            int bytes = driver.getPageSource().getBytes("UTF-8").length;
            WebElement productText = driver.findElement(By.className("productText"));
            product.setDescription(productText.getText());
            product.setSizeBytes(bytes);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }

}
