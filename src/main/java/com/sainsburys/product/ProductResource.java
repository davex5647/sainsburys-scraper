package com.sainsburys.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonPropertyOrder({"title", "size", "unitPrice", "description"})
class ProductResource {
    private String title;
    private int sizeBytes;
    private String unitPrice;
    private String description;

    public ProductResource(String title, String unitPrice) {
        this.title = title;
        this.unitPrice = unitPrice;
    }

    public String getTitle() {
        return title;
    }

    @JsonProperty("unit_price")
    public String getUnitPriceString() {
        return unitPrice;
    }

    @JsonIgnore
    public double getUnitPrice() {
        return Double.parseDouble(unitPrice.replace("£", ""));
    }

    public String getDescription() {
        return description;
    }

    @JsonProperty("size")
    public String getSizeKbString() {
        String sizeKb = Double.toString(sizeBytes / 1000);
        return sizeKb + "kb";
    }

    public void setSizeBytes(int sizeBytes) {
        this.sizeBytes = sizeBytes;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
