package com.sainsburys.util;


import com.sainsburys.main.Application;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class WebDriverHelper {

    public static WebDriver getWebDriver() {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setJavascriptEnabled(true);
        desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, loadPhantomJsPathProperty());

        WebDriver driver=new PhantomJSDriver(desiredCapabilities);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return driver;
    }

    private static String loadPhantomJsPathProperty() {
        Properties prop = new Properties();
        InputStream input = null;

        try {
            String filename = "product-scraper.properties";
            input = Application.class.getClassLoader().getResourceAsStream(filename);
            if(input==null){
                System.out.println("Cannot find properties file: " + filename);
            }

            prop.load(input);
            return prop.getProperty("phantomjs.executable.path");

        } catch (IOException ex) {
            ex.printStackTrace();
        } finally{
            if(input!=null){
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }

}
