package com.sainsburys.product;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ProductReaderTest {

    private static final String RIPE_FRUITS_PAGE_URL = "http://www.sainsburys.co.uk/webapp/wcs/stores/servlet/CategoryDisplay?listView=true&orderBy=FAVOURITES_FIRST&parent_category_rn=12518&top_category=12518&langId=44&beginIndex=0&pageSize=20&catalogId=10137&searchTerm=&categoryId=185749&listId=&storeId=10151&promotionId=#langId=44&storeId=10151&catalogId=10137&categoryId=185749&parent_category_rn=12518&top_category=12518&pageSize=20&orderBy=FAVOURITES_FIRST&searchTerm=&beginIndex=0&hideFilters=true";

    // TODO - Given more time I'd make these tests work offline, using mocking e.g. with wiremock

    @Test
    public void getProductsFromPageReturnsProductsResource() {
        ProductReader productReader = new ProductReader();

        ProductsResource products = productReader.getProductsFromPage(RIPE_FRUITS_PAGE_URL);

        assertThat(products.getProducts().size(), is(15));
        assertThat(products.getTotal() > 0, is(true));
    }

    @Test
    public void getProductsFromPageNonProductPageReturnsNoProducts() {
        ProductReader productReader = new ProductReader();

        ProductsResource products = productReader.getProductsFromPage("http://www.google.com");
        assertThat(products.getProducts().size(), is(0));
    }

}