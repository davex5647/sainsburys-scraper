package com.sainsburys.product;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class ProductResourceTest {

    // TODO - this is basic - given more time I would deserialise the objects back from json to test them

    private static final String PRODUCT_ONE_TITLE = "product one";
    private static final String PRODUCT_TWO_TITLE = "product two";

    @Test
    public void getJsonReturnsJsonStringWithMultipleProducts() {
        ProductsResource productsResource = getProductsResourceWithTwoProducts();

        String json = ProductsResource.getJson(productsResource);

        assertThat(json, containsString("\"title\":\"" + PRODUCT_ONE_TITLE + "\""));
        assertThat(json, containsString("\"title\":\"" + PRODUCT_TWO_TITLE + "\""));
    }

    @Test
    public void getJsonReturnsJsonStringWithTotal() {
        ProductsResource productsResource = getProductsResourceWithTwoProducts();

        String json = ProductsResource.getJson(productsResource);

        assertThat(json, containsString("\"total\":4.49"));
    }

    private ProductsResource getProductsResourceWithTwoProducts() {
        List<ProductResource> products = new ArrayList<>();
        ProductResource productOne = new ProductResource(PRODUCT_ONE_TITLE, "£1.99");
        ProductResource productTwo = new ProductResource(PRODUCT_TWO_TITLE, "£2.50");
        products.add(productOne);
        products.add(productTwo);
        return new ProductsResource(products);
    }

}